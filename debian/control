Source: openjpeg2
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>
Section: libs
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               dh-apache2 <!pkg.openjpeg2.noapache>,
               help2man,
               libcurl4-gnutls-dev | libcurl-ssl-dev,
               libfcgi-dev,
               liblcms2-dev,
               libpng-dev,
               libtiff-dev,
               zlib1g-dev
Build-Depends-Indep: default-jdk,
                     doxygen,
                     javahelper,
                     libxerces2-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/openjpeg2
Vcs-Git: https://salsa.debian.org/debian-phototools-team/openjpeg2.git
Homepage: https://www.openjpeg.org
Rules-Requires-Root: no

Package: libopenjp2-7-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libopenjp2-7 (= ${binary:Version}),
         ${misc:Depends}
Description: development files for OpenJPEG, a JPEG 2000 image library
 OpenJPEG is a library for handling the JPEG 2000 image compression format.
 JPEG 2000 is a wavelet-based image compression standard and permits progressive
 transmission by pixel and resolution accuracy for progressive downloads of an
 encoded image. It supports lossless and lossy compression, supports higher
 compression than JPEG 1991, and has resilience to errors in the image.
 .
 This package contains the development files for openjpeg 2.x

Package: libopenjp2-7
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: JPEG 2000 image compression/decompression library
 OpenJPEG is a library for handling the JPEG 2000 image compression format.
 JPEG 2000 is a wavelet-based image compression standard and permits progressive
 transmission by pixel and resolution accuracy for progressive downloads of an
 encoded image. It supports lossless and lossy compression, supports higher
 compression than JPEG 1991, and has resilience to errors in the image.
 .
 This package contains the runtime files for openjpeg 2.x

Package: openjpeg-doc
Architecture: all
Section: doc
Depends: doc-base,
         ${misc:Depends}
Description: JPEG 2000 image compression/decompression library
 OpenJPEG is a library for handling the JPEG 2000 image compression format.
 JPEG 2000 is a wavelet-based image compression standard and permits progressive
 transmission by pixel and resolution accuracy for progressive downloads of an
 encoded image. It supports lossless and lossy compression, supports higher
 compression than JPEG 1991, and has resilience to errors in the image.
 .
 This package installs the documentation files.

Package: libopenjpip7
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: JPEG 2000 Interactive Protocol
 OpenJPEG is a library for handling the JPEG 2000 image compression format.
 JPEG 2000 is a wavelet-based image compression standard and permits progressive
 transmission by pixel and resolution accuracy for progressive downloads of an
 encoded image. It supports lossless and lossy compression, supports higher
 compression than JPEG 1991, and has resilience to errors in the image.
 .
 This is an implementation of the JPEG 2000 Interactive Protocol (Part-9)

Package: libopenjpip-dec-server
Architecture: any
Multi-Arch: foreign
Section: graphics
Depends: ${misc:Depends},
         ${shlibs:Depends}
Conflicts: openjpip-dec-server
Description: tool to allow caching of JPEG 2000 files using JPIP protocol
 This is client side application for caching remote JPEG 2000 using the JPIP
 protocol. This command line application needs to run on the client side to
 allow application such as opj_viewer to view images.

Package: libopenjpip-viewer
Architecture: all
Build-Profiles: <!pkg.openjpeg2.noapache>
Section: graphics
Depends: libopenjpip-dec-server,
         ${java:Depends},
         ${misc:Depends}
Suggests: libopenjp2-tools
Conflicts: openjpip-viewer
Description: JPEG 2000 java based viewer for advanced remote JPIP access
 Java based client to view remote JPEG 2000 using JPIP protocol.
 This is a simple java viewer to allow:
 .
  - Scale up request: Enlarge the window
  - ROI request: Select a region by mouse click and drag, then click inside the
    red frame of the selected region
  - Annotate image with ROI information in XML metadata: Click button "Region
    Of Interest"
  - Open a new window presenting an aligned image with a locally stored image:
    Click button "Image Registration" (Under Construction)

Package: libopenjpip-server
Architecture: any
Multi-Arch: foreign
Section: graphics
Depends: libwww-perl,
         spawn-fcgi,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ${misc:Recommends}
Conflicts: openjpip-server
Description: JPIP server for JPEG 2000 files
 OpenJPIP software is an implementation of JPEG 2000 Part9: Interactivity tools,
 APIs and protocols (JPIP). For more info about JPIP, check the website:
 http://www.jpeg.org/jpeg2000/j2kpart9.html. The current implementation uses
 some results from the 2KAN project (http://www.2kan.org).
 .
 First Version covers:
 .
  - JPT-stream (Tile based) and JPP-stream (Precinct based) media types
  - Session, channels, cache model managements
  - JPIP over HTTP
  - Indexing JPEG 2000 files
  - Embedding XML formatted metadata
  - Region Of Interest (ROI) requests

Package: libopenjp2-tools
Architecture: any
Multi-Arch: foreign
Section: graphics
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: command-line tools using the JPEG 2000 library
 This package provides with command-line tools allowing for conversions between
 several formats and also provides tools for encoding and decoding
 motion-jpeg2000 video formats:
 .
  - opj_decompress: decodes j2k, jp2, and jpt files to pgm, ppm, pnm,
                  pgx, and bmp.
  - opj_compress: encodes pnm, pgm, pgx, bmp, and ppm files to j2k,
                  and jp2.
  - opj_dump: dump information contains in j2k and jp2.
  - index_create: create jp2 with JPIP index file from a j2k file.
  - frames_to_mj2: convert YUV video streams to mj2 format.
  - mj2_to_frames: convert mj2 video streams to YUV format.
  - wrap_j2k_in_mj2: wrap j2k codestreams into mj2 format.
  - extract-j2k-from_mj2: extract j2k codestreams from the mj2 format.
